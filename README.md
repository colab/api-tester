# The Co-Lab API Tester

This API tester allows us to parse through an entire swagger specification (or list of specifications), validating that each endpoint conforms with the spec as expected. This is used in the Co-Lab status monitor.