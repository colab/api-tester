var JSV = require("JSV").JSV.createEnvironment();
var https = require('https');

//step 1: get swagger doc

function getSwaggerDoc(optionsOrURL) {
    return new Promise((resolve, reject) => {
        const req = https.request(optionsOrURL, (response) => {
            let data = '';
            response.on('data', (d) => {data += d;});
            response.on('end', () => {
                try {
                    resolve(JSON.parse(data));
                } catch(e) {
                    reject(e);
                }
            });
        });
        req.end();
        req.on('error', (e) => {reject(e);});
    });
}

const step1 = getSwaggerDoc({
    hostname: 'api.colab.duke.edu',
    path: '/meta/v1/docs',
    headers: {
        'x-api-key': 'api-docs'
    }
});

//step 2: extract each endpoint and its expected responses
function getEndpoints(apis) {
    let endpoints = [];
    for (let name in apis) {
        for (let version in apis[name]) {
            let api = apis[name][version];
            
            for (let path in api.paths) {
                //skip over extensions to the spec, only allowing real paths
                if (path.substring(0,2) == 'x-') { continue; }
                
                for (let method in apis[name][version].paths[path]) {
                    //skip over extensions to the spec, only allowing real methods
                    if (method.substring(0,2) == 'x-') { continue; }
                    
                    let operation = apis[name][version].paths[path][method];
                    //console.log(operation);
                    
                    let params = operation.parameters || [];
                    let requiredParams = [];
                    for (let i = 0; i < params.length; i++) {
                        if (params[i].required) {
                            requiredParams.push(params[i]);
                        }
                    }
                    
                    endpoints.push({
                        host: api.host,
                        path: path,
                        method: method,
                        security: operation.security || path.security || {},
                        requiredParams: requiredParams || [],
                        responses: operation.responses
                    });
                    
                    
                }
            }
        }
    }
    return endpoints;
}

const step2 = step1.then(getEndpoints);
//step 3: make api call to each
const step3 = step2.then((endpoints)=>{
    for (let endpoint of endpoints) {
        console.log(endpoint);
        
    }
});
//step 4: validate that the response was as expected

const step4 = step3.then(()=>{
    //let report = JSV.validate(json,schema);
    //if (report.errors.length > 0) {
    //  we got ourselves an issue
    //}
});

//step -1: catch errors

step4.catch((err) => {
    console.error(err);
});
